<?php
namespace AIGames\PHPBot;

/**
 * Class AI
 * @package AIGames\PHPBot
 */
class AI
{

    private $settings;
    private $winrules;

    /**
     * AI constructor.
     * @param Map $map
     */
    public function __construct(Map $map)
    {
        $this->map = $map;

        for ($y = 0; $y < 3; $y++) {
            for ($x = 0; $x < 3; $x++) {
                $x1 = $x * 3;
                $y1 = $y * 3;
                $rules = array(
                    array(array($x1 + 0, $y1 + 0), array($x1 + 1, $y1 + 0), array($x1 + 2, $y1 + 0)),
                    array(array($x1 + 0, $y1 + 1), array($x1 + 1, $y1 + 1), array($x1 + 2, $y1 + 1)),
                    array(array($x1 + 0, $y1 + 2), array($x1 + 1, $y1 + 2), array($x1 + 2, $y1 + 2)),

                    array(array($x1 + 0, $y1 + 0), array($x1 + 0, $y1 + 1), array($x1 + 0, $y1 + 2)),
                    array(array($x1 + 1, $y1 + 0), array($x1 + 1, $y1 + 1), array($x1 + 1, $y1 + 2)),
                    array(array($x1 + 2, $y1 + 0), array($x1 + 2, $y1 + 1), array($x1 + 2, $y1 + 2)),

                    array(array($x1 + 0, $y1 + 0), array($x1 + 1, $y1 + 1), array($x1 + 2, $y1 + 2)),
                    array(array($x1 + 2, $y1 + 0), array($x1 + 1, $y1 + 1), array($x1 + 0, $y1 + 2)),
                );
                $this->winrules[] = $rules;
            }
        }

    }

    public function updateSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * Work through a complicated algorithm that reduces the number of available moves to
     * a path for winning scenario that takes into consideration every possible situation
     * and outputs one single move action
     *
     * No... not really. It chooses a random square... for now.
     *
     * @todo Timing
     *
     */
    public function move()
    {
        //$this->settings['your_botid']);
        $availableMoves = $this->map->getAvailableMoves();
        //$boardWin = $this->getboardWin('op');
        $topWinningSqureBig = $this->getTopWinningSuqreBig('my');

        if (!empty($topWinningSqureBig)) {
            $bstmove = $this->getBestMove($availableMoves, 'my', 2);

        }
        if (empty($bstmove)) {
            $topWinningSqureBig = $this->getTopWinningSuqreBig('op');
            $BestMoveByBig = $this->getBestMoveByBig($topWinningSqureBig);
            if (!empty($BestMoveByBig)) {
                $relTopSqures = $this->getRelTopSqures($topWinningSqureBig);
                $availableMoves = $this->removeTopOp($availableMoves, $relTopSqures);
            }
        }

        $availableMoves = $this->removeUnWanted($availableMoves);

        if (empty($bstmove)) {
            $bstmove = $this->getBestMove($availableMoves, 'my', 2);
        }

        if (empty($bstmove)) {

            $availableMoves = $this->filterBySecondHighest($availableMoves);

            $bstmove = $this->getBestMove($availableMoves, 'op', 2);
        }

        if (empty($bstmove)) {
            $bstmove = $this->getBestMove($availableMoves, 'my', 1);
        }
        if (empty($bstmove)) {
            $bstmove = $this->getBestMove($availableMoves, 'op', 1);
        }

        /** Make a random move
         * @var Move $move
         */
        if (empty($bstmove)) {
            $bstmove = $availableMoves[rand(0, count($availableMoves) - 1)];
        }

        return $bstmove;
    }
    public function filterBySecondHighest($availableMoves)
    {
        $BigSqures = $this->getBigBoardSqureByPos($availableMoves);
        for ($i = 0; $i < count($BigSqures); $i++) {
            $BestMoveByBig = $this->getBestMoveByBig($BigSqures[$i]);
            if (!empty($BestMoveByBig)) {
                $relTopSqures = $this->getRelTopSqures($BigSqures[$i]);
                $availableMoves = $this->removeTopOp($availableMoves, $relTopSqures);
            }
        }
        return $availableMoves;
    }

    public function getBestMoveByBig($topWinningSqureBig)
    {
        //$topWinningSqureBig = array(2, 0);
        $pos = $this->converPostionToSqure($topWinningSqureBig);
        $board = $this->getBoardBySqure($topWinningSqureBig);
        $boardFilter = $this->filterBoardByMove($board, 'op');
        $BestMoveByBig = $this->getWinnnigSqureSmall($boardFilter, $pos);
        return $BestMoveByBig;
        //print_r(array($topWinningSqureBig, $pos, $board));

    }
    public function filterBoardByMove($board, $player)
    {
        $botId = $this->checkBot($player);
        $boardFilter = array();
        for ($i = 0; $i < count($board); $i++) {
            if ($this->map->field[$board[$i][0]][$board[$i][1]] == $botId) {
                $boardFilter[] = $board[$i];
            }
        }
        return $boardFilter;

    }
    public function getBoardBySqure($BigSqure)
    {
        $board = array();
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 3; $j++) {
                $board[] = array($BigSqure[0] * 3 + $j, $BigSqure[1] * 3 + $i);
            }
        }
        return $board;
    }

    public function removeUnWanted($availableMoves)
    {
        $boardWin = $this->getboardWin('my');
        for ($i = 0; $i < count($boardWin); $i++) {
            $squrePos = $this->converSqureToPostion($boardWin[$i]);
            $relTopSqures = $this->getRelTopSqures($squrePos);
            $availableMoves = $this->removeTopOp($availableMoves, $relTopSqures);
        }

        $boardWin = $this->getboardWin('op');

        for ($i = 0; $i < count($boardWin); $i++) {
            $squrePos = $this->converSqureToPostion($boardWin[$i]);
            $relTopSqures = $this->getRelTopSqures($squrePos);
            //print_r(array($relTopSqures, "AASSS"));
            $availableMoves = $this->removeTopOp($availableMoves, $relTopSqures);
        }
        return $availableMoves;

    }
    public function removeTopOp($availableMoves, $relTopSqures)
    {

        if (empty($relTopSqures)) {
            return $availableMoves;
        } else {

            $filteredAvailable = array();
            for ($i = 0; $i < count($availableMoves); $i++) {
                $matched = false;
                for ($j = 0; $j < count($relTopSqures); $j++) {
                    if ($availableMoves[$i]->getX() == $relTopSqures[$j][0] && $availableMoves[$i]->getY() == $relTopSqures[$j][1]) {
                        $matched = true;
                    }

                }
                if (!$matched) {
                    $filteredAvailable[] = $availableMoves[$i];
                }

            }
            if (!empty($filteredAvailable)) {
                return $filteredAvailable;
            } else {
                return $availableMoves;
            }

        }

    }
    public function getRelTopSqures($topWinningSqureBig)
    {
        $relarr = array();
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 3; $j++) {
                $x1 = $j * 3;
                $y1 = $i * 3;
                $relarr[] = array($topWinningSqureBig[0] + $x1, $topWinningSqureBig[1] + $y1);
            }
        }
        return $relarr;
    }

    public function getBestMove($availableMoves, $player, $lvl)
    {
        $botId = $this->checkBot($player);
        $aviMoveCount = count($availableMoves);
        for ($i = 0; $i < $aviMoveCount; $i++) {
            for ($j = 0; $j < count($this->winrules); $j++) {
                for ($k = 0; $k < count($this->winrules[$j]); $k++) {
                    $matchBot = 0;
                    $matchFree = 0;
                    for ($l = 0; $l < 3; $l++) {
                        if ($availableMoves[$i]->getX() == $this->winrules[$j][$k][$l][0] && $availableMoves[$i]->getY() == $this->winrules[$j][$k][$l][1]) {
                            $matchFree++;
                        }
                        if ($this->map->field[$this->winrules[$j][$k][$l][0]][$this->winrules[$j][$k][$l][1]] == $botId) {
                            $matchBot++;
                        }
                    }
                    if ($matchBot == $lvl && 1 == $matchFree) {
                        return $availableMoves[$i];

                    }

                }
            }
        }

    }
    public function getboardWin($player)
    {
        $botId = $this->checkBot($player);
        $boardWin = array();
        for ($j = 0; $j < count($this->winrules); $j++) {
            for ($k = 0; $k < count($this->winrules[$j]); $k++) {
                $matchBot = 0;
                for ($l = 0; $l < 3; $l++) {
                    if ($this->map->field[$this->winrules[$j][$k][$l][0]][$this->winrules[$j][$k][$l][1]] == $botId) {
                        $matchBot++;
                    }
                }
                if (3 == $matchBot) {
                    $boardWin[] = $j;
                }

            }
        }
        return $boardWin;

    }

    public function getTopWinningSuqreBig($player)
    {
        $boardWin = $this->getboardWin($player);
        $boardWin = $this->BigBoardToPos($boardWin);
        return $this->getWinnnigSqureSmall($boardWin, 0);
    }
    public function BigBoardToPos($boardWin)
    {
        $board = array();
        for ($i = 0; $i < count($boardWin); $i++) {
            $board[] = $this->converSqureToPostion($boardWin[$i]);
        }
        return $board;
    }
    public function getWinnnigSqureSmall($boardWin, $bigSqurePos)
    {
        for ($k = 0; $k < count($this->winrules[$bigSqurePos]); $k++) {
            $matchBot = 0;
            $matched = array();
            for ($l = 0; $l < 3; $l++) {
                for ($i = 0; $i < count($boardWin); $i++) {
                    //$squrePos = $this->converSqureToPostion($boardWin[$i]);
                    if ($this->winrules[$bigSqurePos][$k][$l][0] == $boardWin[$i][0] && $this->winrules[$bigSqurePos][$k][$l][1] == $boardWin[$i][1]) {
                        $matchBot++;
                        $matched[] = $l;
                    }
                }
            }
            if (2 == $matchBot) {
                $unMatchedVal = $this->getunMatchedposValue($matched);
                return array($this->winrules[$bigSqurePos][$k][$unMatchedVal][0], $this->winrules[$bigSqurePos][$k][$unMatchedVal][1]);
            }

        }
    }
    public function getunMatchedposValue($matched)
    {
        if ((0 == $matched[0] || 0 == $matched[1]) && (1 == $matched[0] || 1 == $matched[1])) {
            return 2;
        }
        if ((0 == $matched[0] || 0 == $matched[1]) && (2 == $matched[0] || 2 == $matched[1])) {
            return 1;
        }
        if ((1 == $matched[0] || 1 == $matched[1]) && (2 == $matched[0] || 2 == $matched[1])) {
            return 0;
        }
    }

    public function converSqureToPostion($squre)
    {
        if (0 == $squre) {
            return array(0, 0);
        }
        if (1 == $squre) {
            return array(1, 0);
        }
        if (2 == $squre) {
            return array(2, 0);
        }
        if (3 == $squre) {
            return array(0, 1);
        }
        if (4 == $squre) {
            return array(1, 1);
        }
        if (5 == $squre) {
            return array(2, 1);
        }
        if (6 == $squre) {
            return array(0, 2);
        }
        if (7 == $squre) {
            return array(1, 2);
        }
        if (8 == $squre) {
            return array(2, 2);
        }
    }

    public function converPostionToSqure($pos)
    {
        if (0 == $pos[0] && 0 == $pos[1]) {
            return 0;
        }
        if (1 == $pos[0] && 0 == $pos[1]) {
            return 1;
        }
        if (2 == $pos[0] && 0 == $pos[1]) {
            return 2;
        }
        if (0 == $pos[0] && 1 == $pos[1]) {
            return 3;
        }
        if (1 == $pos[0] && 1 == $pos[1]) {
            return 4;
        }
        if (2 == $pos[0] && 1 == $pos[1]) {
            return 5;
        }
        if (0 == $pos[0] && 2 == $pos[1]) {
            return 6;
        }
        if (1 == $pos[0] && 2 == $pos[1]) {
            return 7;
        }
        if (2 == $pos[0] && 2 == $pos[1]) {
            return 8;
        }
    }
    public function getBigBoardSqureByPos($availableMoves)
    {
        $BigSqures = array();
        for ($i = 0; $i < count($availableMoves); $i++) {
            if ($availableMoves[$i]->getX() >= 0 && $availableMoves[$i]->getX() < 3 && $availableMoves[$i]->getY() >= 0 && $availableMoves[$i]->getY() < 3) {
                $BigSqures[] = 0;
            }
            if ($availableMoves[$i]->getX() >= 3 && $availableMoves[$i]->getX() < 6 && $availableMoves[$i]->getY() >= 0 && $availableMoves[$i]->getY() < 3) {
                $BigSqures[] = 1;
            }
            if ($availableMoves[$i]->getX() >= 6 && $availableMoves[$i]->getX() < 9 && $availableMoves[$i]->getY() >= 0 && $availableMoves[$i]->getY() < 3) {
                $BigSqures[] = 2;
            }
            if ($availableMoves[$i]->getX() >= 0 && $availableMoves[$i]->getX() < 3 && $availableMoves[$i]->getY() >= 3 && $availableMoves[$i]->getY() < 6) {
                $BigSqures[] = 3;
            }
            if ($availableMoves[$i]->getX() >= 3 && $availableMoves[$i]->getX() < 6 && $availableMoves[$i]->getY() >= 3 && $availableMoves[$i]->getY() < 6) {
                $BigSqures[] = 4;
            }
            if ($availableMoves[$i]->getX() >= 6 && $availableMoves[$i]->getX() < 9 && $availableMoves[$i]->getY() >= 3 && $availableMoves[$i]->getY() < 6) {
                $BigSqures[] = 5;
            }
            if ($availableMoves[$i]->getX() >= 0 && $availableMoves[$i]->getX() < 3 && $availableMoves[$i]->getY() >= 6 && $availableMoves[$i]->getY() < 9) {
                $BigSqures[] = 6;
            }
            if ($availableMoves[$i]->getX() >= 3 && $availableMoves[$i]->getX() < 6 && $availableMoves[$i]->getY() >= 6 && $availableMoves[$i]->getY() < 9) {
                $BigSqures[] = 7;
            }
            if ($availableMoves[$i]->getX() >= 6 && $availableMoves[$i]->getX() < 9 && $availableMoves[$i]->getY() >= 5 && $availableMoves[$i]->getY() < 9) {
                $BigSqures[] = 8;
            }
        }
        return array_unique($BigSqures);
    }

    public function checkBot($player)
    {
        if ('my' == $player) {
            return $this->settings['your_botid'];
        } else {
            return (1 == $this->settings['your_botid']) ? 2 : 1;
        }

    }

}
