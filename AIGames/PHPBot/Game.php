<?php
namespace AIGames\PHPBot;

/**
 * Class Game
 * @package AIGames\PHPBot
 */
class Game
{
    private $settings = [];
    private $map;
    private $validSettings = ['timebank', 'time_per_move', 'player_names', 'your_bot', 'your_botid'];

    public function __construct()
    {
        $this->map = new Map();
        $this->ai = new AI($this->map);

        /* $this->settings['timebank'] = '10000';
    $this->settings['time_per_move'] = '500';
    $this->settings['player_names'] = 'player1,player2';
    $this->settings['your_bot'] = 'player1';
    $this->settings['your_botid'] = '1';
    $this->ai->updateSettings($this->settings);
    $this->map->setFieldFromString("0,1,0,2,0,0,0,0,2,0,1,0,2,0,1,0,1,0,0,0,0,2,0,0,2,2,2,0,0,1,0,2,1,2,0,0,0,0,0,0,2,0,0,2,1,1,0,0,0,2,0,0,0,2,0,1,1,0,1,0,0,0,1,2,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0");
    $this->map->setMacroBoardFromString("-1,2,2,-1,2,2,-1,-1,-1");*/
    }

    /**
     * Returns the requested setting variable
     * @param $setting
     * @return null
     */
    public function get($setting)
    {
        if (array_key_exists($setting, $this->settings)) {
            return $this->settings[$setting];
        } else {
            return null;
        }
    }

    /**
     * Main Function that continuously receives STDIN input
     */
    public function run()
    {
        //$this->makeMove();
        //$input = "settings hello ssdsd";
        //preg_match("/^(?<command>settings|update game|action|custom) (?<arg>\S+) (?<parameter>.+)$/", $input, $commandParams);
        //print_r(array($input, $commandParams));die;
        $handle = fopen("php://stdin", "r");
        while (1) {
            $command = trim(fgets($handle));
            if ("" == $command) {
                continue;
            }

            if ("exit" == $command) {
                $this->output("bye");
                break;
            }

            $this->processInput($command);

        }
    }

    public function output($string)
    {
        echo $string . "\n";
    }

    /**
     * Processes the command input and calls the required method.
     * Returns true if command is accepted, false if the command is unrecognized
     *
     * @param $input
     * @return bool
     */
    public function processInput($input)
    {

        preg_match("/^(?<command>settings|update game|action|custom) (?<arg>\S+) (?<parameter>.+)$/", $input, $commandParams);
        if (!count($commandParams)) {
            $this->output("Undefined Command");

            return false;
        }

        if ('settings' === $commandParams['command']) {
            if (false === $this->set($commandParams['arg'], $commandParams['parameter'])) {
                $this->output("Undefined setting");

                return true;
            }
        }

        if ('update game' === $commandParams['command']) {
            if ('field' === $commandParams['arg']) {
                return $this->map->setFieldFromString($commandParams['parameter']);
            }

            if ('macroboard' === $commandParams['arg']) {
                return $this->map->setMacroBoardFromString($commandParams['parameter']);
            }
        }

        if ('action' === $commandParams['command']) {
            if ('move' === $commandParams['arg']) {
                $this->makeMove();

                return true;
            }
        }

        if ('custom' === $commandParams['command']) {
            if ('print' === $commandParams['arg']) {
                if ('field' === $commandParams['parameter']) {
                    $this->map->printField();

                    return true;
                }
                if ('macroboard' === $commandParams['parameter']) {
                    $this->map->printMacroBoard();

                    return true;
                }
            }

        }

        return false;
    }

    /**
     * Receives a game setting and stores it. Returns false if setting is not recognized
     *
     * @param $setting
     * @param $value
     * @return bool
     */
    public function set($setting, $value)
    {
        if (!in_array($setting, $this->validSettings)) {
            return false;
        }

        $this->settings[$setting] = $value;

        //Update the AI of the change:
        $this->ai->updateSettings($this->settings);

        return true;
    }

    /**
     * Invokes the AI, provides it with the game state and runs the move command
     */
    public function makeMove()
    {
        /**
         * @var Move $move
         */
        $move = $this->ai->move();

        $this->output("place_move " . $move->getX() . " " . $move->getY());
    }
}
